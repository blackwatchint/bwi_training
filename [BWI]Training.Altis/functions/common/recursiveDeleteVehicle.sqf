private _unit = _this;

if (vehicle _unit == _unit) then
{
	deleteVehicle _unit;
}
else
{
	deleteVehicle (vehicle _unit);
	[ { _this call bwi_fnc_recursiveDeleteVehicle; }, _unit] call CBA_fnc_execNextFrame;
};