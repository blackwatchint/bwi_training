/* 
* Takes a string of a vehicle class name and returns
* the size of a bounding spehere around the vehicle
*/
params["_vehicleClass"];
private["_tempVic", "_result"];

_tempVic = createVehicle [_vehicleClass, [0,0,0], [], 0, "NONE"];
_result = sizeOf _vehicleClass;
deleteVehicle _tempVic;
_result