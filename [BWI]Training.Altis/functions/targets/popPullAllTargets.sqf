params ["_group", "_popState"];
private ["_idx", "_groupName", "_targets"];

_groupName = configName _group;

_idx = PV_bwi_targetGroupNames find _groupName;

_targets = PV_bwi_groupTargets select _idx;
// pops or pulls targets depending on given popstate (1=unpoped,0=poped)
{
	private _target = _x;
	_target animate["terc", _popState];
} forEach _targets;