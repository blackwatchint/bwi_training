private ["_targetGroups"];

//gets all subclasses of "CfgTeleporters"
_targetGroups = [(missionConfigFile >> "CfgTargets"),0, true, true ] call BIS_fnc_returnChildren;

// Variables below are "public" variables. No broadcast needed because the function runs on client.
// these will serve as a sort of dictionary<string, object[]> targetGroupName holds the GroupNames(Key) and groupTargets holds the targets for each group(Value) indexof(targetGroupName) == indexof (targetsinthatgroup)
PV_bwi_targetGroupNames = [];
PV_bwi_groupTargets = [];
PV_bwi_activeSequences = [];
nopop=true;

//Create main action for later use
_MainAction = ["BWI_MainActions", "Interactions", "\a3\ui_f\data\IGUI\Cfg\Actions\eject_ca.paa", {}, {true}, {}, [], [0,0,0.5], 10] call ace_interact_menu_fnc_createAction;

// init for each target group
{
	private ["_group", "_groupName", "_type", "_description", "_controlVicVarName", "_controlVic", "_deactivateAllAction", "_activateAllAction", "_sequenceActiveNode", "_startSequenceAction", "_groupNode"];

	_group = _x;

	_type = getNumber(_group >> "type");
	if (isNil "_type") exitWith 
	{
		 systemChat format ["No type found at %1", _group]; 
	};

	_description = getText(_group >> "description");
	if (isNil "_description") exitWith 
	{
		 systemChat format ["No description found at %1", _group]; 
	};

	// adding description to groupNames
	_groupName = configName _x;
	PV_bwi_targetGroupNames pushBack _groupName;
	
	_controlVicVarName = getText(_group >> "controlVehicle");
	if (isNil "_controlVicVarName") exitWith 
	{
		 systemChat format ["No controlVehicle found at %1", _group]; 
	};

	_controlVic =  missionNamespace getVariable [_controlVicVarName , objNull];
	if (isNil "_controlVic") exitWith 
	{
		 systemChat format ["No sourceVehicle for ""%1""", _controlVicVarName]; 
	};


	//Create actions for later use
	_deactivateAllAction = ["deactivateAllAction", "Deactivate all targets", "", { _this select 2 call bwi_fnc_popPullAllTargets; }, { !((_this select 2 select 2) in PV_bwi_activeSequences) }, {}, [_group, 1, _groupName], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
	_activateAllAction = ["activateAllAction", "Activate all targets", "", {  _this select 2 call bwi_fnc_popPullAllTargets; }, { !((_this select 2 select 2) in PV_bwi_activeSequences) }, {}, [_group, 0, _groupName], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
	_sequenceActiveNode = ["sequenceActiveNode", "Sequence is active!", "", { true }, { (_this select 2 in PV_bwi_activeSequences) }, {}, _groupName, [0,0,0], 10] call ace_interact_menu_fnc_createAction;
	_startSequenceAction = ["startSequenceAction", "Start sequence", "", {  _this select 2 call bwi_fnc_popPullAllTargets; [_this select 2 select 0] spawn bwi_fnc_startSequence; }, { !((_this select 2 select 2) in PV_bwi_activeSequences) }, {}, [_group, 1, _groupName], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
	_groupNode = [_groupName, _description, "", {}, { true }, {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction;

	if (_type > 1) then 
	{
		private ["_targetNames", "_targets"];

		_targetNames = [_group, "targets", nil] call BIS_fnc_returnConfigEntry;
		_targets = [];

		{
			private _tar = missionNamespace getVariable [_x , objNull];
			_targets pushback _tar;
		} forEach _targetNames;
		
		PV_bwi_groupTargets pushBack _targets;
	}
	else
	{
		private ["_sourceVicVarName", "_sourceVic", "_radius"];

		_sourceVicVarName = getText(_group >> "sourceVehicle");
		if (isNil "_sourceVicVarName") exitWith 
		{
			systemChat format ["No sourceVehicle found at %1", _group]; 
		};

		_sourceVic =  missionNamespace getVariable [_sourceVicVarName , objNull];
		if (isNil "_sourceVic") exitWith 
		{
			systemChat format ["No sourceVehicle for ""%1""", _sourceVicVarName]; 
		};

		_radius = getNumber(_group >> "radius");
		if (isNil "_radius") exitWith 
		{
			systemChat format ["No radius found at %1", _group]; 
		};

		//adds targets to groupTargets
		PV_bwi_groupTargets pushBack (position _sourceVic nearObjects ["TargetP_Inf_F",_radius ]);
	};
	
	//adds actions to control entity
	[_controlVic, _MainAction, []] call bwi_fnc_addActionToObjectIfUnique;
	[_controlVic, 0, ["BWI_MainActions"], _groupNode] call ace_interact_menu_fnc_addActionToObject;
	[_controlVic, 0, ["BWI_MainActions",_groupName], _deactivateAllAction] call ace_interact_menu_fnc_addActionToObject;
	[_controlVic, 0, ["BWI_MainActions",_groupName], _activateAllAction] call ace_interact_menu_fnc_addActionToObject;
	[_controlVic, 0, ["BWI_MainActions",_groupName], _startSequenceAction] call ace_interact_menu_fnc_addActionToObject;
	[_controlVic, 0, ["BWI_MainActions",_groupName], _sequenceActiveNode] call ace_interact_menu_fnc_addActionToObject;

}forEach _targetGroups;