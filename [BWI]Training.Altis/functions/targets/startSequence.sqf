params ["_group"];
private ["_number", "_time", "_concurrent", "_groupName", "_idx", "_targets"];

_number = getNumber(_group >> "number");

_time = getNumber(_group >> "time");

_concurrent = getNumber(_group >> "concurrent");

_groupName = configName _group;

_idx = PV_bwi_targetGroupNames find _groupName;

//List this sequence in the array of active ones
PV_bwi_activeSequences = PV_bwi_activeSequences + [_groupName];
publicVariable "PV_bwi_activeSequences";

_targets = PV_bwi_groupTargets select _idx;

// for loop for number of sets
for [{_n=0}, {_n < _number}, {_n=_n+1}] do
{
	private ["_concurrentTargets", "_viableTargets"];

	_concurrentTargets = [];

	//Make a copy of all the targets we are allowed to pop so we can remove any that have already been added
	_viableTargets = + _targets;

	// for loop for concurrent targets
	for [{_i=0}, {_i< _concurrent}, {_i=_i+1}] do
	{
		//Select a random target from our array of allowed targets and then remove it from allowed to avoid duplicates
		private _target = selectRandom _viableTargets;
		_viableTargets = _viableTargets - [_target];
		_concurrentTargets pushback _target;
	};

	//pop up
	{
		_x animate["terc", 0];
	} forEach _concurrentTargets;
	
	sleep _time;

	//pop back down
	{
		_x animate["terc", 1];
	} forEach _concurrentTargets;
};

//Remove sequence as active
PV_bwi_activeSequences = PV_bwi_activeSequences - [_groupName];
publicVariable "PV_bwi_activeSequences";