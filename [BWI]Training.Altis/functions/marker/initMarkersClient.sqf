params ["_actionGroups"];

//Main action creation
private _MainAction = ["BWI_MainActions", "Interactions", "\a3\ui_f\data\IGUI\Cfg\Actions\eject_ca.paa", {}, {true}, {}, [], [0,0,0.5], 10] call ace_interact_menu_fnc_createAction;

//Activate action creation
private _ActivateAction = ["ActivateAction", "Show Markers", "", {}, {true}, {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction;

//Deactivate action creation
private _DeactivateAction = ["DeactivateAction", "Hide Markers", "", {}, 
{ 
	if (isNil "PV_bwi_activeMarkers") exitWith {false};
	if (count PV_bwi_activeMarkers == 0) exitWith {false};
	private _condition = (_this select 1) getVariable ["isInstructor", false];
	_condition
}, 
{
	//Deactivation subaction creation
	params ["_target", "_player", "_params"];
	private _childActions = [];

	private _deactivateAllAction = ["DeactivateAllAction", "All markers", "",
	{ 
		//Call deactivate function without param to clear all
		"all" call bwi_fnc_deactivateMarkerGroup;
	}, 
	{true},	{}, ""] call ace_interact_menu_fnc_createAction;
	_childActions pushBack [_deactivateAllAction, [], _target];

	{
		private _markersToActivate = _x select 0;
		private _deactivatedAlpha = _x select 1;
		private _actionTitle = _x select 2;
		
		private _deactivateGroupAction = ["DeactivateGroupAction", _actionTitle, "",
		{ 
			//Call deactivate function with action param array
			_this select 2 call bwi_fnc_deactivateMarkerGroup;
		}, 
		{true}, {}, [_x]] call ace_interact_menu_fnc_createAction;
		_childActions pushBack [_deactivateGroupAction, [], _target];
	}forEach PV_bwi_activeMarkers;

	_childActions
}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction;

//Activation subaction creation
{
	private _markerArray = _x select 0;
	private _activatedAlpha = _x select 1;
	private _deactivatedAlpha = _x select 2;
	private _clearOldMarkers = _x select 3;
	private _activationPoints = _x select 4;
	private _actionTitle = _x select 5;

	private _ActivateGroupAction = ["ActivateGroupAction", _actionTitle, "", 
	{ 
		//Call activate function with action param array
		_this select 2 call bwi_fnc_activateMarkerGroup;
	}, 
	{ 
		if (isNil "PV_bwi_activeMarkers") then 
		{
			PV_bwi_activeMarkers = [];
			publicVariable "PV_bwi_activeMarkers";
		};
		if ([_this select 2 select 0, _this select 2 select 2, _this select 2 select 4] in PV_bwi_activeMarkers) exitWith {false};
		private _condition = (_this select 1) getVariable ["isInstructor", false];
		_condition
	}, 
	{}, [_markerArray, _activatedAlpha, _deactivatedAlpha, _clearOldMarkers, _actionTitle], [0,0,0], 10] call ace_interact_menu_fnc_createAction;

	//Add the action for swithcing to this marker group (and any missing base actions) to all the defined activation points
	{
		//Add any missing base actions
		[_x, _MainAction, []] call bwi_fnc_addActionToObjectIfUnique;
		[_x, _ActivateAction, ["BWI_MainActions"]] call bwi_fnc_addActionToObjectIfUnique;
		[_x, _DeactivateAction, ["BWI_MainActions"]] call bwi_fnc_addActionToObjectIfUnique;
		//Add the group action for group currently being handled
		[_x, 0, ["BWI_MainActions", "ActivateAction"], _ActivateGroupAction] call ace_interact_menu_fnc_addActionToObject;
	}forEach _activationPoints;
}forEach _actionGroups;


[
	[
		["mkr_tankrange_1","mkr_tankrange_2","mkr_tankrange_3","mkr_tankrange_4","mkr_tankrange_5","mkr_tankrange_6","mkr_tankrange_pos_1","mkr_tankrange_pos_2","mkr_tankrange_pos_3","mkr_tankrange_7","mkr_tankrange_8","mkr_tankrange_9","mkr_tankrange_10"
		]
		,0,"Armor Range"
	]
]