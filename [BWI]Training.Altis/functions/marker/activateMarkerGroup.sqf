params ["_markersToActivate", "_activatedAlpha", "_deactivatedAlpha", "_clearOldMarkers", "_actionTitle"];
private ["_activateEachMarker"];

//Give us some peace of mind that the function will be executed with proper variables
if (count _this != 5) exitWith { systemChat "Wrong number of parameters in bwi_fnc_activateMarkerGroup"; };
if (typeName _markersToActivate != "ARRAY") exitWith { systemChat "Wrong typeName for markersToActivate in bwi_fnc_activateMarkerGroup"; };
if (typeName _clearOldMarkers != "BOOL") exitWith { systemChat "Wrong typeName for clearOldMarkers in bwi_fnc_activateMarkerGroup"; };
if (typeName _activatedAlpha != "SCALAR" || typeName _deactivatedAlpha != "SCALAR") exitWith { systemChat "Wrong typeName for Alpha in bwi_fnc_activateMarkerGroup"; };
if (_activatedAlpha > 1 || _activatedAlpha < 0 || _deactivatedAlpha > 1 || _deactivatedAlpha < 0) exitWith { systemChat "Alpha out of range in bwi_fnc_activateMarkerGroup"; };

//Define small recurring function which loops over the marker array and sets them to activated alpha value
_activateEachMarker = 
{
	_x setMarkerAlpha _activatedAlpha;
}forEach _markersToActivate;

//Check if we need to remove old markers before we add the new ones
if (_clearOldMarkers) then
{
	//Call removal function and wait until finished. If it returns false there were no active markers
	call bwi_fnc_deactivateMarkerGroup;
};
call _activateEachMarker;

//Set our new array of markers as the activeMarkers array or add the new markers (also note the deactivated alpha for use in deactivateMarkerGroup)
if (isNil "PV_bwi_activeMarkers") then
{
	PV_bwi_activeMarkers = [[_markersToActivate, _deactivatedAlpha, _actionTitle]];
}
else
{
	PV_bwi_activeMarkers pushBack [_markersToActivate, _deactivatedAlpha, _actionTitle];
};
publicVariable "PV_bwi_activeMarkers";