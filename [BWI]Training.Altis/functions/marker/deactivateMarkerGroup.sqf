/*
	This function can be called to remove all currently active markers and set them to their deactivated alpha by passing "all".
	A parameter can be passed to only hide a specific set of markers. The function returns true after it has finished. The return value 
	can be used as a check to trigger other	actions at the correct time, an example being calling this function, waiting for the finish, 
	and then activating	new markers.
*/
private _activeMarkersToDeactivate = _this;

//If there is no active markers then there is nothing to deactivate
if (isNil "PV_bwi_activeMarkers") exitWith {false};

//If "all" passed, clear all
if (typeName _activeMarkersToDeactivate == "STRING") then
{
	if (_activeMarkersToDeactivate == "all") then
	{
		_activeMarkersToDeactivate = + PV_bwi_activeMarkers;
	};
};

{
	private _markersToDeactivate = _x select 0;
	private _deactivatedAlpha = _x select 1;

	{
		_x setMarkerAlpha (_deactivatedAlpha);
	}forEach _markersToDeactivate;

	//Remove marker group from the PV
	PV_bwi_activeMarkers = PV_bwi_activeMarkers - [_x];
}forEach _activeMarkersToDeactivate;

//Broadcast new group of active markers
publicVariable "PV_bwi_activeMarkers";

//Return true to report that the function has finished
true