private ["_MarkerConfigs", "_actionGroups"];

_MarkerConfigs = [(missionConfigFile >> "CfgMarkerActivation"),0, true] call BIS_fnc_returnChildren;

_actionGroups = [];

{
	private ["_activationPointsConfig", "_activationPoints", "_markerPrefix", "_actionTitle", "_markerArray", "_deactivatedAlpha", "_activatedAlpha", "_clearOldMarkers", "_newActionGroup"];

	//Get the action name
	_actionTitle = [_x, "actionTitle", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK against invalid configs
	if (isNil "_actionTitle") exitWith { format ["No actionTitle found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (typename _actionTitle != "STRING") exitWith { format ["Wrong typeName for actionTitle found at %1", _x] remoteExecCall ["systemChat", 0, false]; };

	//Get alpha values
	_deactivatedAlpha = [_x, "deactivatedAlpha", nil] call BIS_fnc_returnConfigEntry;
	_activatedAlpha = [_x, "activatedAlpha", nil] call BIS_fnc_returnConfigEntry;
	//Get default values if none were defined
	if (isNil "_deactivatedAlpha") then
	{
		_deactivatedAlpha = 0;
	};
	if (isNil "_activatedAlpha") then
	{
		_activatedAlpha = 1;
	};	

	//Check if old markers should be cleared when these markers are activated
	_clearOldMarkers = [_x, "clearOldMarkers", nil] call BIS_fnc_returnConfigEntry;
	//Get default value if none was defined
	if (isNil "_clearOldMarkers") then
	{
		_clearOldMarkers = false;
	}
	else
	{
		//Arma stop converting my config entries please for the love of god
		if (_clearOldMarkers == "true") then
		{
			_clearOldMarkers = true;
		}
		else
		{
			_clearOldMarkers = false;
		};
	};
	
	//Get the config array of activationPoints
	_activationPointsConfig = [_x, "activationPoints", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK against empty configs
	if (isNil "_activationPointsConfig") exitWith { format ["No activationPoints found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	//Convert activationPointsConfig array to object references
	_activationPoints = [];
	{
		private ["_activationPointTemp"];

		//Get reference to the actual object
		_activationPointTemp = missionNamespace getVariable [_x, nil];
		//ERROR CHECK against invalid config entries
		if (isNil "_activationPointTemp") exitWith { format ["Invalid activationPoint found called %1", _x] remoteExecCall ["systemChat", 0, false]; };
		_activationPoints = _activationPoints + [_activationPointTemp];
	}forEach _activationPointsConfig;

	//Get the marker prefix
	_markerPrefix = [_x, "markerPrefix", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK against invalid configs
	if (isNil "_markerPrefix") exitWith { format ["No markerPrefix found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (typename _markerPrefix != "STRING") exitWith { format ["Wrong typeName for markerPrefix found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	
	//Get all markers with prefix
	_markerArray = [];
	{
		//a = toArray "strings112"; b = toArray "string"; c = count b; d = count a - count b; a deleteRange [c, d]; a = toString a; b = toString b; e = a isEqualTo b
		private ["_checkPrefix", "_checkMarker", "_prefixLength", "_lengthDifference"];

		//Convert strings to array so we can manipulate them
		_checkPrefix = toArray _markerPrefix;
		_checkMarker = toArray _x;

		//Index of last character + 1 (same as length) of prefix is where we need to stop checking
		_prefixLength = count _checkPrefix;
		//Length difference is the amount of characters we need to remove to turn the marker into the prefix
		_lengthDifference = count _checkMarker - _prefixLength;

		//Remove all characters after the part where the prefix should be
		_checkMarker deleteRange [_prefixLength, _lengthDifference];
		//Convert back to string so we can check if they are the same
		_checkPrefix = toString _checkPrefix;
		_checkMarker = toString _checkMarker;
		
		//If the changed checkMarker is equal to the prefix it means it started with the prefix and should be added to our array of markers
		if ( _checkMarker == _checkPrefix ) then 
		{
			_markerArray pushBack _x;
			_x setMarkerAlpha _deactivatedAlpha;
		};
	}forEach allMapMarkers;

	_newActionGroup = [_markerArray, _activatedAlpha, _deactivatedAlpha, _clearOldMarkers, _activationPoints, _actionTitle];
	_actionGroups pushBack _newActionGroup;

}forEach _MarkerConfigs;

//Call the function to add actions for all clients
[_actionGroups] remoteExecCall ["bwi_fnc_initMarkersClient", [0,-2] select isDedicated, true];