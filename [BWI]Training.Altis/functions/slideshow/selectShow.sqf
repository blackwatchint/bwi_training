params ["_Projector", "_ShowIndex"];
private ["_AvailableSlideShows", "_RequestedSlideShow", "_ActiveSlides"];

//Get our configs
_AvailableSlideShows = _Projector getVariable "AvailableSlideShows";
_RequestedSlideShow = _AvailableSlideShows select _ShowIndex;
_ActiveSlides = [_RequestedSlideShow, "Slides", nil] call BIS_fnc_returnConfigEntry;
//ERROR CHECK against invalid config
if (isNil "_ActiveSlides") exitWith { systemChat format ["Invalid Slides found at %1", _SlideShow ]; };
if (typeName _ActiveSlides != "ARRAY") exitWith { systemChat format ["Wrong typeName for Slides at %1", _SlideShow ]; };
if (count _ActiveSlides < 1) exitWith { systemChat format ["Slides is an empty array at %1", _SlideShow ]; };

//Set Object Vars
_Projector setVariable ["ActiveSlideShow", _ShowIndex, true];
_Projector setVariable ["ActiveSlide", 0, true];
_Projector setObjectTextureGlobal [0, _ActiveSlides select 0];