private ["_ProjectorConfigs"];

//gets all subclasses of "CfgProjectors"
_ProjectorConfigs = [(missionConfigFile >> "CfgProjectors"),0, true] call BIS_fnc_returnChildren;

//Loop over the projectors
{
	private ["_ProjectorVar", "_ProjectorVarConfig", "_AttachedShows", "_ActiveSlides", "_AvailableSlideShows", "_i"];
	
	//Get values from the config
	_ProjectorVarConfig = [_x, "ProjectorVar", nil] call BIS_fnc_returnConfigEntry;
	_AttachedShows = [_x, "AttachedShows", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK against empty configs
	if (isNil "_ProjectorVarConfig") exitWith { format ["Server: No ProjectorVar found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	
	//Get reference to the actual object
	_ProjectorVar = missionNamespace getVariable [_ProjectorVarConfig, nil];
	//ERROR CHECK against invalid config entries
	if (isNil "_ProjectorVar") exitWith { format ["Server: Invalid ProjectorVar found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (isNil "_AttachedShows") exitWith { format ["Server: Invalid AttachedShows found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (typeName _AttachedShows != "ARRAY") exitWith { format ["Server: Wrong typeName for AttachedShows at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (count _AttachedShows < 1) exitWith { format ["Server: AttachedShows is an empty array at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	
	//Get slideshow slides config
	_ActiveSlides = [(missionConfigFile >> "CfgSlideShows" >> _AttachedShows select 0), "Slides", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK invalid slides
	if (isNil "_ActiveSlides") exitWith { format ["Server: No slides found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (typeName _ActiveSlides != "ARRAY") exitWith { format ["Server: Wrong typeName for Slides at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (count _ActiveSlides < 1) exitWith { format ["Server: Slides is an empty array at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	
	//Make an array with the configs for all attached shows
	_AvailableSlideShows = [];
	for [ {_i = 0}, {_i < count _AttachedShows}, {_i = _i+1} ] do
	{
		//Check if the slideshow referenced exists
		if (isNull (missionConfigFile >> "CfgSlideShows" >> _AttachedShows select _i)) exitWith { format ["Server: Slideshow %1 referenced by %2 does not exist in CfgSlideShows", [_AttachedShows select _i, _x]] remoteExecCall ["systemChat", 0, false]; };
		//Add the slideshow config to list of available for this projector
		_AvailableSlideShows = _AvailableSlideShows + [missionConfigFile >> "CfgSlideShows" >> _AttachedShows select _i];
	};
	
	//Set starting value for the Projector's active slideshow
	_ProjectorVar setVariable ["AvailableSlideShows", _AvailableSlideShows, true];
	_ProjectorVar setVariable ["ActiveSlideShow", 0, true];
	_ProjectorVar setVariable ["ActiveSlide", 0, true];
	_ProjectorVar setObjectTextureGlobal [0, _ActiveSlides select 0];
					
} forEach _ProjectorConfigs;

//Call the function to add actions for all clients
[] remoteExecCall ["bwi_fnc_initSlideShowsClient", [0,-2] select isDedicated, true];