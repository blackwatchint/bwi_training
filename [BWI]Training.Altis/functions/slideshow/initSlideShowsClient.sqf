private ["_ProjectorConfigs", "_MainAction", "_SelectAction"];

//gets all subclasses of "CfgProjectors"
_ProjectorConfigs = [(missionConfigFile >> "CfgProjectors"),0, true] call BIS_fnc_returnChildren;

//Create actions for future use
_MainAction = ["BWI_MainActions", "Interactions", "\a3\ui_f\data\IGUI\Cfg\Actions\eject_ca.paa", {}, {true}, {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
_SelectAction = ["SelectAction", "Change Slide Show", "", {}, {true}, {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction;

//Loop through the projectors
{
	private ["_ProjectorVarConfig", "_remoteControlsConfig", "_remoteControls", "_NextAction", "_PreviousAction", "_AttachedShows", "_ProjectorVar", "_ShowName", "_i", "_mainActionNeeded", "_attachedActions", "_TempAction"];
	
	//Get values from the config
	_ProjectorVarConfig = [_x, "ProjectorVar", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK against empty configs
	if (isNil "_ProjectorVarConfig") exitWith { systemChat format ["No ProjectorVar found at %1", _x]; };
	
	//Get reference to the actual object
	_ProjectorVar = missionNamespace getVariable [_ProjectorVarConfig, nil];
	//ERROR CHECK against invalid config entries
	if (isNil "_ProjectorVar") exitWith { systemChat format ["Invalid ProjectorVar found at %1", _x]; };
	
	//Get attachedShows
	_AttachedShows = _ProjectorVar getVariable "AvailableSlideShows";
	
	//Get reference to the actual object
	_ProjectorVar = missionNamespace getVariable [_ProjectorVarConfig, nil];
	//ERROR CHECK against invalid config entries
	if (isNil "_ProjectorVar") exitWith { systemChat format ["Invalid ProjectorVar found at %1", _x]; };
	
	//Get values from the config
	_remoteControlsConfig = [_x, "remoteControls", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK against empty configs
	if (isNil "_remoteControlsConfig") exitWith { systemChat format ["No remoteControls found at %1", _x]; };
	if (typeName _remoteControlsConfig != "ARRAY")	exitWith { systemChat format ["Wrong typeName for remoteControls found at %1", _x]; };

	//Get references to the actual objects
	_remoteControls = [];
	{
		private ["_tempRemoteControl"];

		//Get the object and check existance
		_tempRemoteControl = missionNamespace getVariable [_x, nil];
		if (isNil "_tempRemoteControl") exitWith { systemChat format ["Invalid RemoteControl found called %1", _x]; };
		//Add our object to the array
		_remoteControls = _remoteControls + [_tempRemoteControl];
	}foreach _remoteControlsConfig;

	//Create next slide action
	_NextAction = ["NextSlide", "Next Slide", "", 
	{ 
		//call function with action param 0 (projectorvar)
		[_this select 2 select 0, 1] call bwi_fnc_selectSlide; 
	}, 
	{ 
		private ["_Condition", "_Projector", "_AvailableSlideShows", "_CurrentSlideShow", "_ActiveSlide", "_Slides"]; 
	
		_Projector = _this select 2 select 0;
		_AvailableSlideShows = _Projector getVariable "AvailableSlideShows";
		_CurrentSlideShow = _AvailableSlideShows select (_Projector getVariable "ActiveSlideShow");
		_ActiveSlide = _Projector getVariable "ActiveSlide";
		_Slides = [_CurrentSlideShow, "Slides", nil] call BIS_fnc_returnConfigEntry;
	
		//Check if the player is an instructor
		_isInstructor =  _Player getVariable ["isInstructor", false];
	
		//Check if the next slide will be in range and we are an instructor
		_Condition = _isInstructor && (_ActiveSlide + 1 < (count _Slides));
		_Condition
	}, 
	{}, [_ProjectorVar], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
	
	//Create previous slide action
	_PreviousAction = ["PreviousSlide", "Previous Slide", "", 
	{ 
		//call function with action param 0 (projectorvar)
		[_this select 2 select 0, -1] call bwi_fnc_selectSlide; 
	}, 
	{ 
		private ["_Condition", "_Projector", "_AvailableSlideShows", "_CurrentSlideShow", "_ActiveSlide", "_Slides"]; 
	
		_Projector = _this select 2 select 0;
		_AvailableSlideShows = _Projector getVariable "AvailableSlideShows";
		_CurrentSlideShow = _AvailableSlideShows select (_Projector getVariable "ActiveSlideShow");
		_ActiveSlide = _Projector getVariable "ActiveSlide";
		_Slides = [_CurrentSlideShow, "Slides", nil] call BIS_fnc_returnConfigEntry;
	
		//Check if the player is an instructor
		_isInstructor =  _Player getVariable ["isInstructor", false];
	
		//Check if the previous slide will be in range and we are an instructor
		_Condition = _isInstructor && (_ActiveSlide - 1 >= 0);
		_Condition
	}, 
	{}, [_ProjectorVar], [0,0,0], 10] call ace_interact_menu_fnc_createAction;

	//Go over all remotes and add the actions
	{
		private ["_tempRemoteControl"];

		//Make a proper var so we can use it in other loops
		_tempRemoteControl = _x;

		//Add main action
		[_tempRemoteControl, _MainAction, []] call bwi_fnc_addActionToObjectIfUnique;
		//Add next and previous slide action
		[_tempRemoteControl, 0, ["BWI_MainActions"], _NextAction] call ace_interact_menu_fnc_addActionToObject;
		[_tempRemoteControl, 0, ["BWI_MainActions"], _PreviousAction] call ace_interact_menu_fnc_addActionToObject;
		//Add select (main) action
		[_tempRemoteControl, 0, ["BWI_MainActions"], _SelectAction] call ace_interact_menu_fnc_addActionToObject;
		{
			private ["_TempAction"];

			//Get name of the show
			_ShowName = [ _x, "ShowName", nil] call BIS_fnc_returnConfigEntry;
			//ERROR CHECK against empty config entry
			if (isNil "_ShowName") exitWith { systemChat format ["No ShowName found at %1", _x]; };

			//Create the action for switching the slideshow
			_TempAction = [_ShowName, _ShowName, "",
			{
				[_this select 2 select 0, _this select 2 select 1] call bwi_fnc_selectShow;
			}, 
			{
				private ["_Condition", "_OurIndex", "_Projector", "_ActiveSlideShow", "_Player"];
				//Get our variables
				_Projector = _this select 2 select 0;
				_Player = _this select 1;
				_OurIndex = _this select 2 select 1;
				_ActiveSlideShow = _Projector getVariable "ActiveSlideShow";

				//Check if the player is an instructor
				_isInstructor =  _Player getVariable ["isInstructor", false];

				//Check if this slideshow is NOT already active and we are an instructor
				_Condition = _isInstructor && (_ActiveSlideShow != _OurIndex);
				_Condition
			}, {}, [_ProjectorVar, _forEachIndex], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
			
			//Add the switch action to the remote
			[_tempRemoteControl, 0, ["BWI_MainActions", "SelectAction"], _TempAction] call ace_interact_menu_fnc_addActionToObject;
		}forEach _AttachedShows;
	}forEach _remoteControls
} forEach _ProjectorConfigs;