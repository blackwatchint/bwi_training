params ["_Projector", "_change"];
private ["_AvailableSlideShows", "_CurrentSlideShow", "_ActiveSlide", "_Slides"];

//Get our configs
_AvailableSlideShows = _Projector getVariable "AvailableSlideShows";
_CurrentSlideShow = _AvailableSlideShows select (_Projector getVariable "ActiveSlideShow");
_ActiveSlide = _Projector getVariable "ActiveSlide";
_Slides = [_CurrentSlideShow, "Slides", nil] call BIS_fnc_returnConfigEntry;

//If requested change is out of range, ignore (expected double request, interaction option should dissapear on next frame)
if (_ActiveSlide + _change < 0 || _ActiveSlide + _change > (count _Slides -1)) exitWith {};

//Set texture to requested slide
_Projector setObjectTextureGlobal [0, _Slides select (_ActiveSlide + _change) ];
//Set variable holding active slide number
_Projector setVariable ["ActiveSlide", (_ActiveSlide + _change), true];