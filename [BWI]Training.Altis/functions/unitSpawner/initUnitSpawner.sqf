private ["_unitSpawnConfigs", "_unitSpawnActivationPoints", "_groupsToDelete"];

//gets all subclasses of "CfgUnitSpawns"
_unitSpawnConfigs = [(missionConfigFile >> "CfgUnitSpawns"),0, true] call BIS_fnc_returnChildren;

_groupsToDelete = [];

//Used to tell the clients which activation points should be checked
_unitSpawnActivationPoints = [];

//Loop over all of the unit spawn entries
{
	private ["_activationPointsConfig", "_activationPoints", "_groupVarsConfig", "_groupVars", "_groupName", "_unitTypes", "_groupSides", "_unitDirections", "_unitLocations", "_actionGroup", "_groupArray", "_groupIndices"];

	//Get our activation points
	_activationPointsConfig = [_x, "activationPoints", nil] call BIS_fnc_returnConfigEntry;
	if (isNil "_activationPointsConfig") exitWith { format ["Server: No activationPoints found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (typeName _activationPointsConfig != "ARRAY") exitWith { format ["Server: Wrong typeName for activationPoints found at %1", _x] remoteExecCall ["systemChat", 0, false]; };

	//Translate into the real activation points
	_activationPoints = []; 
	{
		private ["_tempActivationPoint"];

		_tempActivationPoint = missionNamespace getVariable [_x, nil];
		if (isNil "_tempActivationPoint") exitWith { format ["Server: No object found with name %1", _x] remoteExecCall ["systemChat", 0, false]; };
		_activationPoints = _activationPoints + [_tempActivationPoint];
	}forEach _activationPointsConfig;

	//Get our cosmetic name
	_groupName = [_x, "groupName", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK 
	if (isNil "_groupName") exitWith { format ["Server: No GroupName found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	if (typeName _groupName != "STRING") exitWith { format ["Server: Wrong typeName for GroupName found at %1", _x] remoteExecCall ["systemChat", 0, false]; };

	//Get values from the config
	_groupVarsConfig = [_x, "groupVars", nil] call BIS_fnc_returnConfigEntry;
	//ERROR CHECK against empty configs
	if (isNil "_groupVarsConfig") exitWith { format ["Server: No GroupVarsConfig found at %1", _x] remoteExecCall ["systemChat", 0, false]; };
	
	//Get actual references instead of name
	_groupArray = [];
	{
		private ["_tempGroupVar", "_tempGroup"];

		_tempGroupVar = missionNamespace getVariable [_x, nil];
		if (isNil "_tempGroupVar") exitWith { format ["Server: No valid GroupVar found called %1", _x] remoteExecCall ["systemChat", 0, false]; };
		
		//Lets make sure we have the group and if we got the group lead, change to the group instead
		switch (typeName _tempGroupVar) do
		{
			case "OBJECT": { _tempGroup = group _tempGroupVar; };
			case "GROUP": { _tempGroup = _tempGroupVar; };
			default { if (true) exitwith { format ["Server: Wrong typeName for GroupVar found called %1", _x] remoteExecCall ["systemChat", 0, false]; }; };
		};

		//Note that we need to remove this group at the end
		if !(_tempGroup in _groupsToDelete) then 
		{
			_groupsToDelete = _groupsToDelete + [_tempGroup];
		};

		_groupArray = _groupArray + [_tempGroup];
	}forEach _groupVarsConfig;	

	//Translate the groups into an array of unit types, locations, and directions. Delete unit when done.
	_unitTypes = [];
	_unitLocations = [];
	_unitDirections = [];
	_groupIndices = [];
	_groupSides = [];

	//Loop over all groups in groupArray
	{
		//Save number of this group to add to _groupIndices
		private _groupIndex = _forEachIndex;
		private _handeledVehicles = [];
		
		//Get the side of the group for when we recreate it
		_groupSides = _groupSides + [ side _x];

		//Loop over all units in group
		{
			private _unitBeingHandled = _x;

			//If infantry handle else handle the vic
			if (vehicle _unitBeingHandled == _unitBeingHandled) then
			{
				//Make entries for actionGroup
				_unitTypes = _unitTypes + [typeOf _unitBeingHandled];
				_unitLocations = _unitLocations + [getPos _unitBeingHandled];
				_unitDirections = _unitDirections + [getDir _unitBeingHandled];
				_groupIndices = _groupIndices + [_groupIndex];
			}
			else
			{
				//Vic not handled yet, lets do it now
				if !( (vehicle _unitBeingHandled) in _handeledVehicles) then
				{
					private ["_tempUnitType", "_vicCrew"];

					//Add this vic to the array of already handled
					_handeledVehicles = _handeledVehicles + [vehicle _unitBeingHandled];

					_vicCrew = fullCrew [(vehicle _unitBeingHandled), "", false];
					//Get class names for each of the crew
					{
						_x set [0, typeOf (_x select 0)];
					}forEach _vicCrew;

					//Make an array with vic class and all crew classes
					_tempUnitType = [[typeOf (vehicle _unitBeingHandled), _vicCrew]];

					//Make entries for actionGroup
					_unitTypes = _unitTypes + _tempUnitType;
					_unitLocations = _unitLocations + [getPos (vehicle _unitBeingHandled)];
					_unitDirections = _unitDirections + [getDir (vehicle _unitBeingHandled)];
					_groupIndices = _groupIndices + [_groupIndex];
				};
			};
		}foreach (units _x);
	}forEach _groupArray;
	
	//All data needed to create addAction is saved in action group
	_actionGroup = [_groupName, _groupSides, _groupIndices, _unitTypes, _unitLocations, _unitDirections];

	//Check we created correct amount of entries
	if !(count _groupIndices == count _unitTypes && count _unitTypes == count _unitLocations && count _unitLocations == count _unitDirections) exitWith 
	{ 
		format ["Server: Number of elements for each element in action group resulting from %1 was not equal", _x] remoteExecCall ["systemChat", 0, false]; 
	};

	//Add the actionGroup as a variable to all of the activationPoints
	{
		private ["_oldVariable", "_newVariable"];

		_oldVariable = _x getVariable ["actionGroups", []];
		_newVariable = _oldVariable + [_actionGroup];
		_x setVariable ["actionGroups", _newVariable, true];

		//Add the activation point to the array clients must loop over
		if !(_x in _unitSpawnActivationPoints) then
		{
			_unitSpawnActivationPoints = _unitSpawnActivationPoints + [_x];
		};
	}forEach _activationPoints;
}foreach _unitSpawnConfigs;

//Loop over all groups to delete
{
	private _groupBeingHandled = _x;
	//Loop over all units to delete
	{
		//Delete vehicles of the unit followed by unit
		[{ time > 0 }, { _this call bwi_fnc_recursiveDeleteVehicle; }, _x] call CBA_fnc_waitUntilAndExecute;
	}forEach units _groupBeingHandled;
	deletegroup _groupBeingHandled;
}forEach _groupsToDelete;

//Server is done doing work, clients are now allowed to do their inits
[_unitSpawnActivationPoints] remoteExecCall ["bwi_fnc_initUnitSpawnerClient", [0,-2] select isDedicated, true];