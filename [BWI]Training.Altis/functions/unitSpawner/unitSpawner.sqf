params["_actionGroup"];
private ["_groupName", "_groupSide", "_groupIndices", "_unitTypes", "_unitLocations", "_unitDirections", "_ourGroups"];

//Note all relevant data
_groupName = _actionGroup select 0;
_groupSides = _actionGroup select 1;
_groupIndices = _actionGroup select 2;
_unitTypes = _actionGroup select 3;
_unitLocations = _actionGroup select 4;
_unitDirections = _actionGroup select 5;

_ourGroups = [];

//Create the amount of groups we will need
for [{ _i=0 }, { _i <= ( _groupIndices select ((count _groupIndices)-1)) }, {_i = _i + 1}] do
{
	private _tempGroup = createGroup [(_groupSides select _i), true];
	_ourGroups = _ourGroups + [_tempGroup];
};

//Loop over all unitTypes
{
	private ["_tempUnitGroupIndex", "_tempUnitGroup", "_tempUnitType", "_tempUnitLocation", "_tempUnitDirection"];
	//Note all relevant data of unit at the current index
	_tempUnitGroupIndex = _groupIndices select _forEachIndex;
	_tempUnitGroup = _ourGroups select _tempUnitGroupIndex;
	_tempUnitType = _unitTypes select _forEachIndex;
	_tempUnitLocation = _unitLocations select _forEachIndex;
	_tempUnitDirection = _unitDirections select _forEachIndex;

	//Handle as infantry if string else its an array for vic
	if (typeName _x == "STRING") then
	{
		private ["_ourUnit"];

		_ourUnit = _tempUnitGroup createUnit [_tempUnitType, _tempUnitLocation, [], 0, "NONE"];
		_ourUnit setDir _tempUnitDirection;	
		//Make sure we are in the group properly
		[_ourUnit] join _tempUnitGroup;

		if (isNil "PV_bwi_activeSpawnedUnits") then
		{
			PV_bwi_activeSpawnedUnits = [_ourUnit];
		}
		else
		{
			PV_bwi_activeSpawnedUnits = PV_bwi_activeSpawnedUnits + [_ourUnit];
		};
	}
	else
	{
		private ["_ourVehicle", "_vehicleClass", "_tempCrew"];

		_vehicleClass = _tempUnitType select 0;
		_tempCrew = _tempUnitType select 1;

		//Create the vehicle
		_ourVehicle = createVehicle [_vehicleClass, _tempUnitLocation, [], 0, "NONE"];
		_ourVehicle setDir _tempUnitDirection;

		

		if (isNil "PV_bwi_activeSpawnedUnits") then
		{
			PV_bwi_activeSpawnedUnits = [_ourVehicle];
		}
		else
		{
			PV_bwi_activeSpawnedUnits = PV_bwi_activeSpawnedUnits + [_ourVehicle];
		};

		//Loop over vehicle crew data
		{
			private ["_crewUnitType", "_crewRole", "_crewCargoIndex", "_crewTurretPath", "_crewTurretBool", "_ourUnit"];

			//Split the data
			_crewUnitType = _x select 0;
			_crewRole = toLower (_x select 1);
			_crewCargoIndex = _x select 2;
			_crewTurretPath = _x select 3;
			_crewTurretBool = _x select 4;

			_ourUnit = _tempUnitGroup createUnit [_crewUnitType, [0,0,0], [], 0, "NONE"];
			//Make sure we are in the group properly
			[_ourUnit] join _tempUnitGroup;

			//Handle depending on role
			switch (_crewRole) do
			{
				case "driver":
				{
					_ourUnit moveInDriver _ourVehicle;
				};
				case "commander":
				{
					_ourUnit moveInCommander _ourVehicle;
				};
				case "gunner":
				{
					_ourUnit moveInGunner _ourVehicle;
				};
				case "turret":
				{
					_ourUnit moveInTurret [_ourVehicle, _crewTurretPath];
				};
				case "cargo":
				{
					_ourUnit moveInCargo [_ourVehicle, _crewCargoIndex];
				};
				default { systemChat format ["UnitSpawner function did not know how to handle %1 role", _crewRole] };
			};

			PV_bwi_activeSpawnedUnits = PV_bwi_activeSpawnedUnits + [_ourUnit];
		}forEach _tempCrew;

		//High in the sky means we gonna fly
		if (_tempUnitLocation select 2 > 5) then
		{
			_ourVehicle setVehiclePosition [_tempUnitLocation, [], 0, "FLY"];
			_ourVehicle engineOn true;
			if (_ourVehicle isKindOf "plane") then
			{
				_ourVehicle setVelocity [(sin _tempUnitDirection * 110), (cos _tempUnitDirection * 110), 0];
			};
		};
	};
}forEach _unitTypes;

{
	//Recenter waypoint 0 (only needed for planes probably but editor does it normally so why not
	[_x, 0] setWaypointPosition [getPos (leader _x),0];
}forEach _ourGroups;

publicVariable "PV_bwi_activeSpawnedUnits";