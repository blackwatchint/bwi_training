params["_unitSpawnActivationPoints"];
private["_MainAction","_spawnGroupNode", "_removeSpawnedAction"];

//Create actions for future use
_MainAction = ["BWI_MainActions", "Interactions", "\a3\ui_f\data\IGUI\Cfg\Actions\eject_ca.paa", {}, {true}, {}, [], [0,0,0.5], 10] call ace_interact_menu_fnc_createAction;
_spawnGroupNode = ["spawnGroupNode", "Spawn", "", {}, {
	private _condition = (_this select 1) getVariable ["isInstructor", false];
	_condition
}, {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
_removeSpawnedAction = ["removeSpawnedAction", "Remove old spawned units and bodies", "", {
	//if below returns true, actions should have been hidden
	if (isNil "PV_bwi_activeSpawnedUnits") exitwith {};

	//Vehicle entries need to be after the crew so they only get deleted on their own if they were unmanned (needed for the ejection seats)
	reverse PV_bwi_activeSpawnedUnits;

	//Loop over all the active units and remove them and their vehicles
	{
		if !(isNull _x) then 
		{
			_x call bwi_fnc_recursiveDeleteVehicle;
		};
	}forEach PV_bwi_activeSpawnedUnits;

	PV_bwi_activeSpawnedUnits = nil;
	publicVariable "PV_bwi_activeSpawnedUnits";
}, { 
	private _condition = (_this select 1) getVariable ["isInstructor", false] && !(isNil "PV_bwi_activeSpawnedUnits");
	_condition 
}, {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction;

{
	private ["_actionGroups", "_activationPoint"];

	_activationPoint = _x;
	//Get the actiongroups to handle for this activation point
	_actionGroups = _activationPoint getVariable ["actionGroups", []];

	//Add general actions to the activationpoint
	[_activationPoint, _MainAction, []] call bwi_fnc_addActionToObjectIfUnique;
	[_activationPoint, 0, ["BWI_MainActions"], _spawnGroupNode] call ace_interact_menu_fnc_addActionToObject;
	[_activationPoint, 0, ["BWI_MainActions"], _removeSpawnedAction] call ace_interact_menu_fnc_addActionToObject;
	
	//Go over the action groups and add the needed actions
	{
		private ["_spawnGroupAction"];

		//Add the action for spawning this group
		_spawnGroupAction = [_x select 0, _x select 0, "", { _this select 2 call bwi_fnc_unitSpawner; }, {true}, {}, [_x], [0,0,0], 10] call ace_interact_menu_fnc_createAction;
		[_activationPoint, 0, ["BWI_MainActions", "spawnGroupNode"], _spawnGroupAction] call ace_interact_menu_fnc_addActionToObject;
	}foreach _actionGroups;
}foreach _unitSpawnActivationPoints;