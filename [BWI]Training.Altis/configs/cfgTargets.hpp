class CfgTargets
{
    /*class TargetGroup1
    {
        // control Vic (Laptop)
        controlVehicle = "cntrlTargets";
        // must be unique
        description = "Test Group";
        
        type = 2;//1=area,2=explicit 

        targets[] = {"targetVarName1","targetVarName2"};

        //only if type == 1
        sourceVehicle="source";

        // only if type == 1
        radius = 40;

        // time between targets
        time = 3;

        //number of concurrent targets
        concurrent = 2;

        //number of target sets
        number = 5;
    };*/


    /**
     * Firing Range - All
     */
    class ADV_FiringRange_All_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "All";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_100m_a_1", "tgt_100m_b_1", "tgt_100m_c_1",
            "tgt_200m_a_1", "tgt_200m_b_1", "tgt_200m_c_1",
            "tgt_300m_a_1", "tgt_300m_b_1", "tgt_300m_c_1",
            "tgt_400m_a_1", "tgt_400m_b_1", "tgt_400m_c_1",
            "tgt_600m_a_1", "tgt_600m_b_1", "tgt_600m_c_1",
            "tgt_800m_a_1", "tgt_800m_b_1", "tgt_800m_c_1",
            "tgt_1000m_a_1", "tgt_1000m_b_1", "tgt_1000m_c_1"
        };

        time = 4;
        concurrent = 1;
        number = 9;
    };

   class ADV_FiringRange_All_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "All";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_100m_a_2", "tgt_100m_b_2", "tgt_100m_c_2",
            "tgt_200m_a_2", "tgt_200m_b_2", "tgt_200m_c_2",
            "tgt_300m_a_2", "tgt_300m_b_2", "tgt_300m_c_2",
            "tgt_400m_a_2", "tgt_400m_b_2", "tgt_400m_c_2",
            "tgt_600m_a_2", "tgt_600m_b_2", "tgt_600m_c_2",
            "tgt_800m_a_2", "tgt_800m_b_2", "tgt_800m_c_2",
            "tgt_1000m_a_2", "tgt_1000m_b_2", "tgt_1000m_c_2"
        };

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_All_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "All";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_100m_a_3", "tgt_100m_b_3", "tgt_100m_c_3",
            "tgt_200m_a_3", "tgt_200m_b_3", "tgt_200m_c_3",
            "tgt_300m_a_3", "tgt_300m_b_3", "tgt_300m_c_3",
            "tgt_400m_a_3", "tgt_400m_b_3", "tgt_400m_c_3",
            "tgt_600m_a_3", "tgt_600m_b_3", "tgt_600m_c_3",
            "tgt_800m_a_3", "tgt_800m_b_3", "tgt_800m_c_3",
            "tgt_1000m_a_3", "tgt_1000m_b_3", "tgt_1000m_c_3"
        };

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_All_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "All";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_100m_a_4", "tgt_100m_b_4", "tgt_100m_c_4",
            "tgt_200m_a_4", "tgt_200m_b_4", "tgt_200m_c_4",
            "tgt_300m_a_4", "tgt_300m_b_4", "tgt_300m_c_4",
            "tgt_400m_a_4", "tgt_400m_b_4", "tgt_400m_c_4",
            "tgt_600m_a_4", "tgt_600m_b_4", "tgt_600m_c_4",
            "tgt_800m_a_4", "tgt_800m_b_4", "tgt_800m_c_4",
            "tgt_1000m_a_4", "tgt_1000m_b_4", "tgt_1000m_c_4"
        };

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_All_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "All";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_100m_a_5", "tgt_100m_b_5", "tgt_100m_c_5",
            "tgt_200m_a_5", "tgt_200m_b_5", "tgt_200m_c_5",
            "tgt_300m_a_5", "tgt_300m_b_5", "tgt_300m_c_5",
            "tgt_400m_a_5", "tgt_400m_b_5", "tgt_400m_c_5",
            "tgt_600m_a_5", "tgt_600m_b_5", "tgt_600m_c_5",
            "tgt_800m_a_5", "tgt_800m_b_5", "tgt_800m_c_5",
            "tgt_1000m_a_5", "tgt_1000m_b_5", "tgt_1000m_c_5"
        };

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_All_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "All";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_100m_a_6", "tgt_100m_b_6", "tgt_100m_c_6",
            "tgt_200m_a_6", "tgt_200m_b_6", "tgt_200m_c_6",
            "tgt_300m_a_6", "tgt_300m_b_6", "tgt_300m_c_6",
            "tgt_400m_a_6", "tgt_400m_b_6", "tgt_400m_c_6",
            "tgt_600m_a_6", "tgt_600m_b_6", "tgt_600m_c_6",
            "tgt_800m_a_6", "tgt_800m_b_6", "tgt_800m_c_6",
            "tgt_1000m_a_6", "tgt_1000m_b_6", "tgt_1000m_c_6"
        };

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Firing Range - 100M
     */
    class ADV_FiringRange_100M_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "100M";
        type = 2; //1=area, 2=explicit 

		targets[] = {"tgt_100m_a_1", "tgt_100m_b_1", "tgt_100m_c_1"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_100M_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "100M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_100m_a_2", "tgt_100m_b_2", "tgt_100m_c_2"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_100M_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "100M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_100m_a_3", "tgt_100m_b_3", "tgt_100m_c_3"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_100M_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "100M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_100m_a_4", "tgt_100m_b_4", "tgt_100m_c_4"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_100M_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "100M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_100m_a_5", "tgt_100m_b_5", "tgt_100m_c_5"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_100M_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "100M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_100m_a_6", "tgt_100m_b_6", "tgt_100m_c_6"};

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Firing Range - 200M
     */
    class ADV_FiringRange_200M_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "200M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_200m_a_1", "tgt_200m_b_1", "tgt_200m_c_1"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_200M_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "200M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_200m_a_2", "tgt_200m_b_2", "tgt_200m_c_2"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_200M_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "200M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_200m_a_3", "tgt_200m_b_3", "tgt_200m_c_3"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_200M_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "200M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_200m_a_4", "tgt_200m_b_4", "tgt_200m_c_4"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_200M_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "200M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_200m_a_5", "tgt_200m_b_5", "tgt_200m_c_5"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_200M_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "200M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_200m_a_6", "tgt_200m_b_6", "tgt_200m_c_6"};

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Firing Range - 300M
     */
    class ADV_FiringRange_300M_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "300M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_300m_a_1", "tgt_300m_b_1", "tgt_300m_c_1"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_300M_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "300M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_300m_a_2", "tgt_300m_b_2", "tgt_300m_c_2"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_300M_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "300M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_300m_a_3", "tgt_300m_b_3", "tgt_300m_c_3"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_300M_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "300M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_300m_a_4", "tgt_300m_b_4", "tgt_300m_c_4"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_300M_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "300M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_300m_a_5", "tgt_300m_b_5", "tgt_300m_c_5"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_300M_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "300M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_300m_a_6", "tgt_300m_b_6", "tgt_300m_c_6"};

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Firing Range - 400M
     */
    class ADV_FiringRange_400M_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "400M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_400m_a_1", "tgt_400m_b_1", "tgt_400m_c_1"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_400M_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "400M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_400m_a_2", "tgt_400m_b_2", "tgt_400m_c_2"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_400M_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "400M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_400m_a_3", "tgt_400m_b_3", "tgt_400m_c_3"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_400M_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "400M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_400m_a_4", "tgt_400m_b_4", "tgt_400m_c_4"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_400M_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "400M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_400m_a_5", "tgt_400m_b_5", "tgt_400m_c_5"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_400M_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "400M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_400m_a_6", "tgt_400m_b_6", "tgt_400m_c_6"};

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Firing Range - 600M
     */
    class ADV_FiringRange_600M_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "600M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_600m_a_1", "tgt_600m_b_1", "tgt_600m_c_1"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_600M_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "600M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_600m_a_2", "tgt_600m_b_2", "tgt_600m_c_2"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_600M_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "600M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_600m_a_3", "tgt_600m_b_3", "tgt_600m_c_3"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_600M_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "600M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_600m_a_4", "tgt_600m_b_4", "tgt_600m_c_4"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_600M_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "600M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_600m_a_5", "tgt_600m_b_5", "tgt_600m_c_5"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_600M_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "600M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_600m_a_6", "tgt_600m_b_6", "tgt_600m_c_6"};

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Firing Range - 800M
     */
    class ADV_FiringRange_800M_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "800M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_800m_a_1", "tgt_800m_b_1", "tgt_800m_c_1"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_800M_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "800M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_800m_a_2", "tgt_800m_b_2", "tgt_800m_c_2"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_800M_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "800M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_800m_a_3", "tgt_800m_b_3", "tgt_800m_c_3"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_800M_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "800M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_800m_a_4", "tgt_800m_b_4", "tgt_800m_c_4"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_800M_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "800M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_800m_a_5", "tgt_800m_b_5", "tgt_800m_c_5"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_800M_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "800M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_800m_a_6", "tgt_800m_b_6", "tgt_800m_c_6"};

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Firing Range - 1000M
     */
    class ADV_FiringRange_1000M_Col1
    {
        controlVehicle = "targets_adv_frange_board_1";
        description = "1000M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_1000m_a_1", "tgt_1000m_b_1", "tgt_1000m_c_1"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_1000M_Col2
    {
        controlVehicle = "targets_adv_frange_board_2";
        description = "1000M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_1000m_a_2", "tgt_1000m_b_2", "tgt_1000m_c_2"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_1000M_Col3
    {
        controlVehicle = "targets_adv_frange_board_3";
        description = "1000M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_1000m_a_3", "tgt_1000m_b_3", "tgt_1000m_c_3"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_1000M_Col4
    {
        controlVehicle = "targets_adv_frange_board_4";
        description = "1000M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_1000m_a_4", "tgt_1000m_b_4", "tgt_1000m_c_4"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_1000M_Col5
    {
        controlVehicle = "targets_adv_frange_board_5";
        description = "1000M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_1000m_a_5", "tgt_1000m_b_5", "tgt_1000m_c_5"};

        time = 4;
        concurrent = 1;
        number = 9;
    };

    class ADV_FiringRange_1000M_Col6
    {
        controlVehicle = "targets_adv_frange_board_6";
        description = "1000M";
        type = 2; //1=area, 2=explicit 

        targets[] = {"tgt_1000m_a_6", "tgt_1000m_b_6", "tgt_1000m_c_6"};

        time = 4;
        concurrent = 1;
        number = 9;
    };


    /**
     * Pistol Range
     */
    class ADV_PistolRange
    {
        controlVehicle = "targets_adv_prange_board";
        description = "Targets";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_pst_1", "tgt_pst_2", "tgt_pst_3", "tgt_pst_4", "tgt_pst_5", "tgt_pst_6", "tgt_pst_7",
            "tgt_pst_8", "tgt_pst_9", "tgt_pst_10", "tgt_pst_11", "tgt_pst_12", "tgt_pst_13", "tgt_pst_14"
        };

        time = 6;
        concurrent = 3;
        number = 5;
    };


    /**
     * Killhouse
     */
    class ADV_Killhouse_Gnd1
    {
        controlVehicle = "targets_adv_killhouse_board";
        description = "Ground Entry 1";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_kh_1", "tgt_kh_2", "tgt_kh_3", "tgt_kh_4", "tgt_kh_5", "tgt_kh_6",
            "tgt_kh_7", "tgt_kh_8", "tgt_kh_9", "tgt_kh_14", "tgt_kh_15", "tgt_kh_16",
            "tgt_kh_17", "tgt_kh_18", "tgt_kh_19", "tgt_kh_20", "tgt_kh_24" 
        };

        time = 30;
        concurrent = 12;
        number = 1;
    };

    class ADV_Killhouse_Gnd2
    {
        controlVehicle = "targets_adv_killhouse_board";
        description = "Ground Entry 2";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_kh_1", "tgt_kh_2", "tgt_kh_3", "tgt_kh_4", "tgt_kh_5", "tgt_kh_6",
            "tgt_kh_7", "tgt_kh_8", "tgt_kh_9", "tgt_kh_14", "tgt_kh_15", "tgt_kh_16",
            "tgt_kh_17", "tgt_kh_18", "tgt_kh_19", "tgt_kh_20", "tgt_kh_24" 
        };

        time = 60;
        concurrent = 12;
        number = 1;
    };

    class ADV_Killhouse_Roof1
    {
        controlVehicle = "targets_adv_killhouse_board";
        description = "Roof Entry 1";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_kh_1", "tgt_kh_8", "tgt_kh_9", "tgt_kh_10", "tgt_kh_11", "tgt_kh_12",
            "tgt_kh_13", "tgt_kh_14", "tgt_kh_16", "tgt_kh_17", "tgt_kh_18", "tgt_kh_19",
            "tgt_kh_20", "tgt_kh_21", "tgt_kh_22", "tgt_kh_23", "tgt_kh_25", "tgt_kh_26"
        };

        time = 30;
        concurrent = 12;
        number = 2;
    };

    class ADV_Killhouse_Roof2
    {
        controlVehicle = "targets_adv_killhouse_board";
        description = "Roof Entry 2";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_kh_1", "tgt_kh_8", "tgt_kh_9", "tgt_kh_10", "tgt_kh_11", "tgt_kh_12",
            "tgt_kh_13", "tgt_kh_14", "tgt_kh_16", "tgt_kh_17", "tgt_kh_18", "tgt_kh_19",
            "tgt_kh_20", "tgt_kh_21", "tgt_kh_22", "tgt_kh_23", "tgt_kh_25", "tgt_kh_26"
        };

        time = 60;
        concurrent = 12;
        number = 1;
    };


    /**
     * Tactics
     */
    class ADV_Tactics_Peel
    {
        controlVehicle = "targets_adv_tactics_board";
        description = "Peel";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_tact_peel_1", "tgt_tact_peel_2", "tgt_tact_peel_3", "tgt_tact_peel_4",
            "tgt_tact_peel_5", "tgt_tact_peel_6", "tgt_tact_peel_7" 
        };

        time = 6;
        concurrent = 4;
        number = 10;
    };

    class ADV_Tactics_CenterPeel
    {
        controlVehicle = "targets_adv_tactics_board";
        description = "Center Peel";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_tact_cpeel_1", "tgt_tact_cpeel_2", "tgt_tact_cpeel_3"
        };

        time = 6;
        concurrent = 2;
        number = 10;
    };

    class ADV_Tactics_Bound
    {
        controlVehicle = "targets_adv_tactics_board";
        description = "Bound";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_tact_bound_1", "tgt_tact_bound_2", "tgt_tact_bound_3",
            "tgt_tact_bound_4", "tgt_tact_bound_5"
        };

        time = 6;
        concurrent = 3;
        number = 10;
    };


    /**
     * Tactics
     */
    /*class ARM_IFVCourse
    {
        controlVehicle = "targets_arm_board";
        description = "Target Course";
        type = 2; //1=area, 2=explicit 

        targets[] = {
            "tgt_ifvc_1", "tgt_ifvc_2", "tgt_ifvc_3", "tgt_ifvc_4", "tgt_ifvc_5", "tgt_ifvc_6", "tgt_ifvc_7",
            "tgt_ifvc_8", "tgt_ifvc_9", "tgt_ifvc_10", "tgt_ifvc_11", "tgt_ifvc_12", "tgt_ifvc_13", "tgt_ifvc_14",
            "tgt_ifvc_15", "tgt_ifvc_16", "tgt_ifvc_17", "tgt_ifvc_18", "tgt_ifvc_19", "tgt_ifvc_20", "tgt_ifvc_21",
            "tgt_ifvc_22", "tgt_ifvc_23", "tgt_ifvc_24", "tgt_ifvc_25", "tgt_ifvc_26", "tgt_ifvc_27", "tgt_ifvc_28",
            "tgt_ifvc_29", "tgt_ifvc_30", "tgt_ifvc_31", "tgt_ifvc_32", "tgt_ifvc_33", "tgt_ifvc_34", "tgt_ifvc_35",
            "tgt_ifvc_36", "tgt_ifvc_37", "tgt_ifvc_38", "tgt_ifvc_39", "tgt_ifvc_40", "tgt_ifvc_41", "tgt_ifvc_42"
        };

        time = 60;
        concurrent = 28;
        number = 10;
    };*/
};