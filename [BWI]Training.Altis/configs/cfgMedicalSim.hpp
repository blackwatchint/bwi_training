/*
 * Define medical injury simulation patients and admin objects.
 */
class CfgMedicalSim
{
	class BAS_MedicalSim1 {
		admin = "medical_bas_board_1";
		patients[] = { "med_patient_1", "med_patient_2", "med_patient_3", "med_patient_4" };
	};


	class BAS_MedicalSim2 {
		admin = "medical_bas_board_2";
		patients[] = { "med_patient_5", "med_patient_6" };
	};


	class BAS_MedicalSim3 {
		admin = "medical_bas_board_3";
		patients[] = { "med_patient_7", "med_patient_8", "med_patient_9", "med_patient_10" };
	};


	class BAS_MedicalSim4 {
		admin = "medical_bas_board_4";
		patients[] = { "med_patient_11", "med_patient_12" };
	};
};