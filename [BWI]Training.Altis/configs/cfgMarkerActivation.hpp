/*
 * Define manually activatable markers.
 * DeactivatedAlpha and activatedAlpha can be left out if default values of 0 and 1 should be used.
 * If clearOldMarkers is empty, false is selected.
*/
class CfgMarkerActivation
{
    /**
     * Basic Training
     */
	class convoyMarkers
	{
		actionTitle = "Convoy Route";
		markerPrefix = "mkr_convoy_";
		activationPoints[] = {"markers_bas_convoys_board"};
	};


    /**
     * Advanced Training
	 */
	class landNav0Markers
	{
		actionTitle = "Spotting Towers";
		markerPrefix = "mkr_landnav_twr_";
		activationPoints[] = {"markers_adv_landnav_board"};
	};

	class landNav1Markers
	{
		actionTitle = "Navigation Route 1";
		markerPrefix = "mkr_landnav_r1_";
		activationPoints[] = {"markers_adv_landnav_board"};
	};

	class landNav2Markers
	{
		actionTitle = "Navigation Route 2";
		markerPrefix = "mkr_landnav_r2_";
		activationPoints[] = {"markers_adv_landnav_board"};
	};

	class landNav3Markers
	{
		actionTitle = "Navigation Route 3";
		markerPrefix = "mkr_landnav_r3_";
		activationPoints[] = {"markers_adv_landnav_board"};
	};

	class landNav4Markers
	{
		actionTitle = "Navigation Route 4";
		markerPrefix = "mkr_landnav_r4_";
		activationPoints[] = {"markers_adv_landnav_board"};
	};

	class landNav5Markers
	{
		actionTitle = "Navigation Route 5";
		markerPrefix = "mkr_landnav_r5_";
		activationPoints[] = {"markers_adv_landnav_board"};
	};


    /**
     * Rotary Flight School
     */
	class hpMarkers
	{
		actionTitle = "Helipads";
		markerPrefix = "mkr_helipad_";
		activationPoints[] = {"markers_fs_rw_board", "markers_fs_jt_board"};
	};

	class lzMarkers
	{
		actionTitle = "Landing Zones";
		markerPrefix = "mkr_lz_";
		activationPoints[] = {"markers_fs_rw_board"};
	};

	class slingMarkers
	{
		actionTitle = "Slingloading";
		markerPrefix = "mkr_sling_";
		activationPoints[] = {"markers_fs_rw_board"};
	};

	class fastropeMarkers
	{
		actionTitle = "Fastroping and Helocast";
		markerPrefix = "mkr_fastrope_";
		activationPoints[] = {"markers_fs_rw_board", "markers_fs_jt_board"};
	};


    /**
     * Fixed-Wing Flight School
     */
	class rwyMarkers
	{
		actionTitle = "Runway Indicators";
		markerPrefix = "mkr_runway_";
		activationPoints[] = {"markers_fs_fw_board", "markers_fs_jt_board"};
	};

	class casMarkers
	{
		actionTitle = "CAS Range";
		markerPrefix = "mkr_cas_";
		activationPoints[] = {"markers_fs_fw_board", "markers_fs_jt_board", "markers_fs_rw_board"};
	};

	class paraMarkers
	{
		actionTitle = "Paradrop";
		markerPrefix = "mkr_para_";
		activationPoints[] = {"markers_fs_fw_board", "markers_fs_jt_board", "markers_fs_fw_term_board", "markers_fs_rw_board"};
	};


    /**
     * Artillery School
     */
	class mortarMarkers
	{
		actionTitle = "Mortar Range";
		markerPrefix = "mkr_mortar_";
		activationPoints[] = {"markers_art_board"};
	};


    /**
     * Armor School
     */
	class armorDrivingMarkers
	{
		actionTitle = "Driving Course";
		markerPrefix = "mkr_armorDrivingCourse_";
		activationPoints[] = {"markers_arm_board"};
	};

	class armorGunneryRangeMarkers
	{
		actionTitle = "Gunnery Range";
		markerPrefix = "mkr_armorGunneryRange_";
		activationPoints[] = {"markers_arm_board"};
	};

	class commanderCourseAMarkers
	{
		actionTitle = "Commander Course Route A";
		markerPrefix = "mkr_armorCmdCourseA_";
		activationPoints[] = {"markers_arm_board"};
	};
	
	class commanderCourseBMarkers
	{
		actionTitle = "Commander Course Route B";
		markerPrefix = "mkr_armorCmdCourseB_";
		activationPoints[] = {"markers_arm_board"};
	};


    /**
     * Hostile Range
     */
	class hostileNHotLzMarkers
	{
		actionTitle = "Scenario - Hot LZ (Hotel)";
		markerPrefix = "mkr_scen_nhotlz_";
		activationPoints[] = {"markers_hot_board", "markers_fs_rw_board"};
	};

	class hostileHotLzMarkers
	{
		actionTitle = "Scenario - Hot LZ (Golf)";
		markerPrefix = "mkr_scen_hotlz_";
		activationPoints[] = {"markers_hot_board", "markers_fs_rw_board"};
	};

	class hostileVHotLzMarkers
	{
		actionTitle = "Scenario - Hot LZ (Fox)";
		markerPrefix = "mkr_scen_vhotlz_";
		activationPoints[] = {"markers_hot_board", "markers_fs_rw_board"};
	};

	class hostileFacilitiesMarkers
	{
		actionTitle = "Scenario - Ground Facilities";
		markerPrefix = "mkr_scen_airgnd_";
		activationPoints[] = {"markers_hot_board", "markers_fs_fw_board", "markers_fs_rw_board"};
	};

	class hostileTechnicalsMarkers
	{
		actionTitle = "Scenario - Technical Convoys";
		markerPrefix = "mkr_scen_techs_";
		activationPoints[] = {"markers_hot_board", "markers_fs_fw_board", "markers_fs_rw_board"};
	};

	class hostileMotorMarkers
	{
		actionTitle = "Scenario - Motorised Division";
		markerPrefix = "mkr_scen_motor_";
		activationPoints[] = {"markers_hot_board"};
	};

	class hostileMechMarkers
	{
		actionTitle = "Scenario - Mechanized Division";
		markerPrefix = "mkr_scen_mech_";
		activationPoints[] = {"markers_hot_board"};
	};

	class hostileArmorMarkers
	{
		actionTitle = "Scenario - Armor Division";
		markerPrefix = "mkr_scen_arm_";
		activationPoints[] = {"markers_hot_board"};
	};
};