/*
 * Define slideshow groups for use on projector objects.
 */
class CfgSlideShows 
{
    /**
     * Basic Training
     */
	class BAS_SlideShow1
	{
		ShowName = "Convoys";
		Slides[] = {
			"data\slides\convoys_intro.paa",
			"data\slides\convoys_driving.paa",
			"data\slides\convoys_holding.paa",
			"data\slides\convoys_dismounts.paa",
			"data\slides\convoys_ieds.paa",
			"data\slides\convoys_ieds2.paa", 
			"data\slides\convoys_commands.paa",
		};
	};

	class BAS_SlideShow2
	{
		ShowName = "Medical";
		Slides[] = {
			"data\slides\medical_intro.paa",
			"data\slides\medical_stamina.paa",
			"data\slides\medical_fatigue.paa",
			"data\slides\medical_priorities.paa",
			"data\slides\medical_medevac.paa",
			"data\slides\medical_triage.paa",
			"data\slides\medical_wounds.paa",
			"data\slides\medical_bandages1.paa",
			"data\slides\medical_bandages2.paa",
			"data\slides\medical_vitals.paa",
			"data\slides\medical_medication.paa",
			"data\slides\medical_treatments.paa",
		};
	};

	class BAS_SlideShow3
	{
		ShowName = "Formations";
		Slides[] = {
			"data\slides\formations_intro.paa",
			"data\slides\formations_column.paa",
			"data\slides\formations_line.paa",
			"data\slides\formations_wedge.paa",
			"data\slides\formations_echelon.paa",
			"data\slides\formations_staggered.paa",
			"data\slides\formations_360defence.paa",
			"data\slides\formations_spacing.paa",
		};
	};

	class BAS_SlideShow4
	{
		ShowName = "Radio";
		Slides[] = {
			"data\slides\radio_intro.paa",
			"data\slides\radio_343.paa",
			"data\slides\radio_148-152.paa",
			"data\slides\radio_117.paa",
			"data\slides\radio_terminology1.paa",
			"data\slides\radio_terminology2.paa",
			"data\slides\radio_example.paa",
		};
	};


    /**
     * Advanced Training
     */
	class ADV_SlideShow1
	{
		ShowName = "Land Navigation";
		Slides[] = {
			"data\slides\landnav_intro.paa",
			"data\slides\landnav_basics.paa",
			"data\slides\landnav_6grids.paa",
			"data\slides\landnav_10grids.paa",
			"data\slides\landnav_maptools.paa",
			"data\slides\landnav_analogmt.paa",
			"data\slides\landnav_digitalmt.paa",
			"data\slides\landnav_dagr1.paa",
			"data\slides\landnav_dagr2.paa",
		};
	};

	class ADV_SlideShow2
	{
		ShowName = "Infantry Tactics";
		Slides[] = {
			"data\slides\tactics_intro.paa",
			"data\slides\tactics_basics.paa",
			"data\slides\tactics_traveling1.paa",
			"data\slides\tactics_traveling2.paa",
			"data\slides\tactics_bounding1.paa",
			"data\slides\tactics_bounding2.paa",
			"data\slides\tactics_peeling1.paa",
			"data\slides\tactics_peeling2.paa",
			"data\slides\tactics_crossing.paa",
			"data\slides\tactics_buildings1.paa",
			"data\slides\tactics_buildings2.paa",
			"data\slides\tactics_buildings3.paa",
			"data\slides\tactics_vehicles.paa",
			"data\slides\tactics_armoraware.paa",
		};
	};

	class ADV_SlideShow3
	{
		ShowName = "Infantry Accuracy";
		Slides[] = {
			"data\slides\scopes_intro.paa",
			"data\slides\scopes_infantry.paa",
			"data\slides\scopes_examples1.paa",
			"data\slides\scopes_g36.paa",
			"data\slides\scopes_at.paa",
			"data\slides\scopes_examples2.paa",
			"data\slides\scopes_rpg7.paa",
		};
	};

	class ADV_SlideShow4
	{
		ShowName = "Marksman";
		Slides[] = {
			"data\slides\marksman_intro.paa",
			"data\slides\marksman_dmrvsniper.paa",
			"data\slides\marksman_zeroing.paa",
			"data\slides\marksman_example.paa",
		};
	};

	class ADV_SlideShow5
	{
		ShowName = "Vehicle Identification";
		Slides[] = {
			"data\slides\vehid_intro.paa",
			"data\slides\vehid_report1.paa",
			"data\slides\vehid_report2.paa",
			"data\slides\vehid_cars1.paa",
			"data\slides\vehid_cars2.paa",
			"data\slides\vehid_cars3.paa",
			"data\slides\vehid_mraps1.paa",
			"data\slides\vehid_mraps2.paa",
			"data\slides\vehid_mraps3.paa",
			"data\slides\vehid_apcs1.paa",
			"data\slides\vehid_apcs2.paa",
			"data\slides\vehid_ifvs1.paa",
			"data\slides\vehid_ifvs2.paa",
			"data\slides\vehid_ifvs3.paa",
			"data\slides\vehid_ifvs4.paa",
			"data\slides\vehid_mbts1.paa",
			"data\slides\vehid_mbts2.paa",
			"data\slides\vehid_mbts3.paa",
			"data\slides\vehid_other1.paa",
			"data\slides\vehid_other2.paa",
			"data\slides\vehid_other3.paa",
		};
	};

	class ADV_SlideShow6
	{
		ShowName = "Explosives";
		Slides[] = {
			"data\slides\explosives_intro.paa",
			"data\slides\explosives_triggers.paa",
			"data\slides\explosives_detection.paa",
			"data\slides\explosives_procedure.paa",
			"data\slides\explosives_controlled.paa",
			"data\slides\explosives_improvised.paa",
			"data\slides\explosives_mines.paa",
		};
	};


    /**
     * Rotary Flight School
     */
	class FSRW_SlideShow1
	{
		ShowName = "Rotary C-Grade";
		Slides[] = {
			"data\slides\rotary_c_intro.paa",
			"data\slides\rotary_c_afm.paa",
			"data\slides\rotary_c_gauges.paa",
			"data\slides\rotary_c_stress.paa", 
			"data\slides\rotary_c_orbits.paa", 
			"data\slides\rotary_c_landing.paa", 
			"data\slides\rotary_c_vrs.paa", 
			"data\slides\rotary_c_emergencies.paa", 
			"data\slides\rotary_c_radio1.paa", 
			"data\slides\rotary_c_radio2.paa"
		};
	};

	class FSRW_SlideShow2
	{
		ShowName = "Rotary B-Grade";
		Slides[] = {
			"data\slides\rotary_b_intro.paa",
			"data\slides\rotary_b_radar1.paa",
			"data\slides\rotary_b_radar2.paa", 
			"data\slides\rotary_b_slingloading.paa", 
			"data\slides\rotary_b_insertion.paa", 
			"data\slides\rotary_b_hotlz.paa", 
			"data\slides\rotary_b_speedalt.paa",
			"data\slides\rotary_b_carrier.paa"
		};
	};

	class FSRW_SlideShow3
	{
		ShowName = "Rotary A-Grade";
		Slides[] = {
			"data\slides\rotary_a_intro.paa",
			"data\slides\rotary_a_battlepoints.paa", 
			"data\slides\rotary_a_avoidfriendlies.paa",
			"data\slides\rotary_a_weapons.paa",
			"data\slides\rotary_a_lightattack.paa",
			"data\slides\rotary_a_gunships.paa",
			"data\slides\rotary_a_tactics1.paa",
			"data\slides\rotary_a_tactics2.paa"
		};
	};


    /**
     * Fixed-Wing Flight School
     */
	class FSFW_SlideShow1
	{
		ShowName = "Fixed Wing C-Grade";
		Slides[] = {
			"data\slides\fixed_c_intro.paa",
			"data\slides\fixed_c_hud.paa",
			"data\slides\fixed_c_flaps.paa",
			"data\slides\fixed_c_stalling.paa", 
			"data\slides\fixed_c_runways.paa",
			"data\slides\fixed_c_landing.paa", 
			"data\slides\fixed_c_emergencies.paa", 
			"data\slides\fixed_c_radio1.paa", 
			"data\slides\fixed_c_radio2.paa"
		};
	};

	class FSFW_SlideShow2
	{
		ShowName = "Fixed Wing B-Grade";
		Slides[] = {
			"data\slides\fixed_b_intro.paa",
			"data\slides\fixed_b_radar1.paa",
			"data\slides\fixed_b_radar2.paa", 
			"data\slides\fixed_b_paradrops.paa", 
			"data\slides\fixed_b_airdrops.paa",
			"data\slides\fixed_b_speedalt.paa",
			"data\slides\fixed_b_carrier.paa"
		};
	};

	class FSFW_SlideShow3
	{
		ShowName = "Fixed Wing A-Grade";
		Slides[] = {
			"data\slides\fixed_a_intro.paa",
			"data\slides\fixed_a_battlepoints.paa", 
			"data\slides\fixed_a_avoidfriendlies.paa", 
			"data\slides\fixed_a_weapons1.paa",
			"data\slides\fixed_a_weapons2.paa",
			"data\slides\fixed_a_camera.paa",
			"data\slides\fixed_a_gps.paa"
		};
	};


    /**
     * Joint Tactics Flight School
     */
	class FSJT_SlideShow1
	{
		ShowName = "Joint Tactics J-Grade";
		Slides[] = {
			"data\slides\joint_j_intro.paa",
			"data\slides\joint_j_runways.paa",
			"data\slides\joint_j_radio1.paa",
			"data\slides\joint_j_paradrops.paa",
			"data\slides\joint_j_jumpproc.paa",
			"data\slides\joint_j_rotaryinsertions.paa",
			"data\slides\joint_j_dropproc.paa",
			"data\slides\joint_j_markinglzs.paa",
			"data\slides\joint_j_hotlz.paa",
			"data\slides\joint_j_battlepoints.paa",
			"data\slides\joint_j_avoidfriendlies.paa",
			"data\slides\joint_j_aircraft.paa",
			"data\slides\joint_j_weapons1.paa",
			"data\slides\joint_j_weapons2.paa",
			"data\slides\joint_j_laserlos.paa",
			"data\slides\joint_j_radio2.paa",
			"data\slides\joint_j_pov1.paa",
			"data\slides\joint_j_pov2.paa",
			"data\slides\joint_j_uavcontrol.paa"
		};
	};


    /**
     * Artillery School
     */
	class ART_SlideShow1
	{
		ShowName = "Mortar";
		Slides[] = {
			"data\slides\mortar_intro.paa",
			"data\slides\mortar_basics.paa",
			"data\slides\mortar_azimuth.paa",
			"data\slides\mortar_elevation1.paa",
			"data\slides\mortar_elevation2.paa",
			"data\slides\mortar_adjustments.paa",
			"data\slides\mortar_steps.paa",
			"data\slides\mortar_kestrel.paa",
			"data\slides\mortar_firemissions.paa",
		};
	};

	class ART_SlideShow2
	{
		ShowName = "Artillery";
		Slides[] = {
			"data\slides\artillery_intro.paa",
			"data\slides\artillery_computer.paa",
			"data\slides\artillery_interface.paa",
		};
	};


    /**
     * Armor School
     */
	class ARM_SlideShow1
	{
		ShowName = "Armor";
		Slides[] = {
			"data\slides\armor_intro.paa",
			"data\slides\armor_moving.paa",
			"data\slides\armor_infantryaware.paa",
			"data\slides\armor_engaging.paa",
			"data\slides\armor_weapons1.paa",
			"data\slides\armor_weapons2.paa",
			"data\slides\armor_rounds.paa",
			"data\slides\armor_ranging.paa",
			"data\slides\armor_manualrange.paa",
			"data\slides\armor_commanding.paa",
			"data\slides\armor_sensors.paa",
			"data\slides\armor_tactics.paa",
			"data\slides\armor_terrainuse.paa",
		};
	};
};