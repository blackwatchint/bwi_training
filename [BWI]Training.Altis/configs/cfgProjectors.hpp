/**
 * Define projectors for displaying slideshow groups.
 */
class CfgProjectors
{
	class BAS_Projector
	{
		ProjectorVar = "projector_bas";
		AttachedShows[] = {"BAS_SlideShow1", "BAS_SlideShow2", "BAS_SlideShow3", "BAS_SlideShow4"};
		remoteControls[] = {"remote_bas"};
	};

	class ADV_Projector
	{
		ProjectorVar = "projector_adv";
		AttachedShows[] = {"ADV_SlideShow1", "ADV_SlideShow2", "ADV_SlideShow3", "ADV_SlideShow4", "ADV_SlideShow5", "ADV_SlideShow6"};
		remoteControls[] = {"remote_adv"};
	};

	class FSRW_Projector
	{
		ProjectorVar = "projector_fs_rw";
		AttachedShows[] = {"FSRW_SlideShow1", "FSRW_SlideShow2", "FSRW_SlideShow3"};
		remoteControls[] = {"remote_fs_rw"};
	};

	class FSFW_Projector
	{
		ProjectorVar = "projector_fs_fw";
		AttachedShows[] = {"FSFW_SlideShow1", "FSFW_SlideShow2", "FSFW_SlideShow3"};
		remoteControls[] = {"remote_fs_fw"};
	};

	class FSJT_Projector
	{
		ProjectorVar = "projector_fs_jt";
		AttachedShows[] = {"FSJT_SlideShow1"};
		remoteControls[] = {"remote_fs_jt"};
	};

	class ARM_Projector
	{
		ProjectorVar = "projector_arm";
		AttachedShows[] = {"ARM_SlideShow1"};
		remoteControls[] = {"remote_arm"};
	};

	class ART_Projector
	{
		ProjectorVar = "projector_art";
		AttachedShows[] = {"ART_SlideShow1", "ART_SlideShow2"};
		remoteControls[] = {"remote_art"};
	};
};