class CfgUnitSpawns
{
    /**
     * Rotary-Wing Flight School
     */
	class FSRW_NHotLz
	{
		groupName = "Hot LZ (Hotel)";
		groupVars[] = {
			"enemy_inf_grp_13", "enemy_inf_grp_14", "enemy_inf_grp_15",
			"enemy_inf_grp_16", "enemy_inf_grp_17", "enemy_inf_grp_18",
			"enemy_aa_grp_1", "enemy_aa_grp_2", "enemy_aa_grp_3", "enemy_aa_grp_4"
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_rw_board"};
	};

	class FSRW_HotLz
	{
		groupName = "Hot LZ (Golf)";
		groupVars[] = {
			"enemy_inf_grp_1", "enemy_inf_grp_2", "enemy_inf_grp_3", "enemy_inf_grp_4", "enemy_inf_grp_5", "enemy_inf_grp_6",
			"friend_inf_grp_1", "friend_inf_grp_2", "friend_inf_grp_3", "friend_inf_grp_4",
			"enemy_aa_grp_1"
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_rw_board"};
	};
	
	class FSRW_VHotLz
	{
		groupName = "Hot LZ (Fox)";
		groupVars[] = {
			"enemy_inf_grp_5", "enemy_inf_grp_6", "enemy_inf_grp_7", "enemy_inf_grp_8", "enemy_inf_grp_9", "enemy_inf_grp_10",
			"enemy_aa_grp_1", "enemy_aa_grp_2", "enemy_aa_grp_3"
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_rw_board"};
	};


    /**
     * Fixed-Wing Flight School
     */
	class FSFW_Facilities
	{
		groupName = "Ground Facilities";
		groupVars[] = {
			"enemy_inf_grp_11", "enemy_inf_grp_12",
			"enemy_mech_grp_1", "enemy_mech_grp_2", "enemy_mech_grp_3",
			"enemy_aa_grp_1", "enemy_aa_grp_2", "enemy_aa_grp_3", "enemy_aa_grp_4"
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_fw_board", "scenario_fs_rw_board"};
	};

	class FSFW_Technicals
	{
		groupName = "Technical Convoys";
		groupVars[] = {
			"enemy_tech_grp_1", "enemy_tech_grp_2", "enemy_tech_grp_3", "enemy_tech_grp_4",
			"enemy_tech_grp_5", "enemy_tech_grp_6", "enemy_tech_grp_7", "enemy_tech_grp_8",
			"enemy_tech_grp_9", "enemy_tech_grp_10", "enemy_tech_grp_11", "enemy_tech_grp_12",
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_fw_board", "scenario_fs_rw_board"};
	};

	class FSFW_HeavyAA
	{
		groupName = "Heavy Anti-Air";
		groupVars[] = {
			"enemy_mech_grp_1", "enemy_mech_grp_2", "enemy_mech_grp_3",
			"enemy_aa_grp_1", "enemy_aa_grp_2", "enemy_aa_grp_3", "enemy_aa_grp_4",
			"enemy_aa_grp_5", "enemy_aa_grp_6", "enemy_aa_grp_7", "enemy_aa_grp_8",
			"enemy_inf_grp_16", "enemy_inf_grp_17", "enemy_inf_grp_18",
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_fw_board", "scenario_fs_rw_board"};
	};


    /**
     * Armor School
     */
	class ARM_MotorDivision
	{
		groupName = "Motorised Division";
		groupVars[] = {
			"enemy_motor_grp_1", "enemy_motor_grp_2", "enemy_motor_grp_3", "enemy_motor_grp_4", "enemy_motor_grp_5"
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_fw_board", "scenario_fs_rw_board"};
	};

	class ARM_MechDivision
	{
		groupName = "Mechanized Division";
		groupVars[] = {
			"enemy_mech_grp_2", "enemy_mech_grp_4", "enemy_mech_grp_5", "enemy_mech_grp_6"
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_fw_board", "scenario_fs_rw_board"};
	};

	class ARM_ArmorDivision
	{
		groupName = "Armor Division";
		groupVars[] = {
			"enemy_arm_grp_1", "enemy_arm_grp_2", "enemy_arm_grp_3", "enemy_arm_grp_4"
		};
		activationPoints[] = {"scenario_hot_board", "scenario_fs_fw_board", "scenario_fs_rw_board"};
	};
};