if ( isServer ) then {
	// Initialize slideshows system (server-only).
	[] call bwi_fnc_initSlideShows;

	//Initialize unitspawner system (server-only).
	[] call bwi_fnc_initUnitSpawner;

	//Initialize Markers system (server-only).
	[] call bwi_fnc_initMarkers;
};


if ( hasInterface ) then {
	// Initialize medical simulation system (all non-HC clients).
	[] call bwi_fnc_initMedicalSim;

	// Initialize target system (all non-HC clients).
	[] call bwi_fnc_initTargets;
};