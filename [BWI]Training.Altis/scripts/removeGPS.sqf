params ["_unit"];

// Exit if not local.
if ( !local _unit ) exitWith {};

// Track removed devices.
if ( isNil "bwi_training_gpsDevices" ) then {
	bwi_training_gpsDevices = [];
};

// Remove land navigation cheat devices.
if ( "ItemGPS" in items _unit || "ItemGPS" in assignedItems _unit )   then {
	_unit unassignItem "ItemGPS";
	_unit removeItem "ItemGPS";
	bwi_training_gpsDevices pushBackUnique "ItemGPS";
};

if ( "BWA3_ItemNaviPad" in items _unit || "BWA3_ItemNaviPad" in assignedItems _unit )   then {
	_unit unassignItem "BWA3_ItemNaviPad";
	_unit removeItem "BWA3_ItemNaviPad";
	bwi_training_gpsDevices pushBackUnique "BWA3_ItemNaviPad";
};

if ( "ACE_DAGR" in items _unit )   then {
	_unit removeItem "ACE_DAGR";
	bwi_training_gpsDevices pushBackUnique "ACE_DAGR";
};

if ( "ACE_microDAGR" in items _unit )   then {
	_unit removeItem "ACE_microDAGR";
	bwi_training_gpsDevices pushBackUnique "ACE_microDAGR";
};

if ( "ACE_Kestrel4500" in items _unit )   then {
	_unit removeItem "ACE_Kestrel4500";
	bwi_training_gpsDevices pushBackUnique "ACE_Kestrel4500";
};

//systemChat format ["Removed: %1", bwi_training_gpsDevices];