params ["_unit"];

// Exit if not local.
if ( !local _unit ) exitWith {};

// Restore devices.
if ( !isNil "bwi_training_gpsDevices" && count bwi_training_gpsDevices > 0 ) then {
	{
		// Link or add according to device.
		if ( _x in ["ItemGPS", "BWA3_ItemNaviPad"] ) then {
			_unit linkItem _x;
		} else {
			_unit addItem _x;
		};
	} forEach bwi_training_gpsDevices;

	//systemChat format ["Restored: %1", bwi_training_gpsDevices];

	// Clear the device tracker.
	bwi_training_gpsDevices = [];
};

