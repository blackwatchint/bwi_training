**29MAY2022**
- Add armory panel on Freedom


**01MAR2021**
- Fixed Falcon used instead of Eagle on J-Grade CAS Radio slide
- Fixed Misaligned slides in J-Grade slideshow
- Fixed Default Armory faction not defined in settings
- Fixed AN-2 takes damage when spawned at fixed-wing hangars
- Changed Commander course targets
- Changed Commander course markers
- Changed Armor target range
- Added teleporter from Advanced Training to Explosives and AT ranges
- Added variety to medical test subject colours
- Updated medical slides
- Updated slides for armor school
- Updated mission to work with new addon features
- Updated formation slides


**06MAR2020**
- Updated settings to maintain modpack compatibility.
- Removed ACRE named channels module.


**30AUG2019**
- Added helicopter spawns to the Carrier.
- Added vehicle spawners to Cargo Terminal.
- Added "Technical Convoys" scenario to hot zone.
- Added "Heavy Anti-Air" scenario to hot zone.
- Added and improved slides for flight school.
- Added and improved slides for medical training.
- Added default settings for BWI Armory and BWI Motorpool.
- Improved existing hot zone scenarios (excluding "Hot LZ").
- Improved all existing hot zone marker groups.
- Replaced all vehicle spawners with BWI Motorpool instances.
- Replaced resupply at Cargo Terminal with BWI Resupply instance.
- Fixed broken medical dummies at medical training area.
- Fixed player spawns to work with new BWI Armory format.
- Removed slingloading spawners (use Cargo Terminal).
- Removed UAVs at Joint Training Center.
- Removed fixed-wing Rearm Point.
- Removed CAS Helipads.
- Removed non-existant modules.


**02AUG2018**
- Added AV8-B Harrier, F/A-18 Hornet and SU-35 Flanker to spawnable aircraft.
- Replaced all targets on the Anti-Tank Range with vehicle targets.
- Fixed target sequences failing after multiple sequences.
- Removed Eurofighter Typhoon from spawnable aircraft.


**14MAR2018**
- Added visible targets to all landing zones.
- Added Medical Supplies to cargo terminal spawner.
- Changed marker boards to allow individual marker groups to be hidden.
- Replaced ACE Settings modules with CBA Settings.
- Reduced number of lights at helipads to reduce collision glitches.
- Fixed Accessories crate at firing range not responding to inventory actions.
- Fixed gates at Explosives Compound not being interactable.
- Removed Mi-24 from CAS helipads spawners due to ground clipping.


**07DEC2017**
- Added Eurofighter Typhoon to hangar vehicle spawner.
- Updated Explosives triggers slide to indicate clacker range limit.
- Removed F-14 Tomcat from hangar vehicle spawner.


**02DEC2017**
- Added another 8 instructor/trainee slots.
- Added more medical test patients to the medical practical area.
- Added a second "combat medical" test to the medical practical area.
- Added a medical tent to the medical practical area.
- Added more wrecks and trash to the convoy practical route.
- Added four (defused) IEDs to the convoy practical route.


**30NOV2017**
- Added more positions and numbered indicators to the vehicle ID practice.
- Added more vehicles to the vehicle ID spawners, broken into three groups by position.
- Added accessories crate to the firing range.
- Added armory board to the land navigation training area.
- Added tower map marker to Land Navigation marker board.
- Added "Reverse" sign to the armor driving course.
- Added G36 scope example to the Infantry Accuracy slideshow.
- Added ticker-tape to explosives range to indicate safe zone.
- Added additional targets to the mortar range.
- Added four destructible buildings to the armor firing range.
- Updated Medical slideshow with new information on bandages.
- Updated Vehicle ID slideshow with all-new slides.
- Updated Infantry Tactics slideshow with all-new slides.
- Updated Armor slideshow to include unguided rockets.
- Updated Mortar adjustments slide to cover variations.
- Improved the vehicle variety available at all vehicle spawners.
- Changed the "IFV course" to be longer, better balanced and more MBT compatible.
- Changed height of targets at AT firing range to match an MBT.
- Changed timing on the tactics practice area to run longer.
- Changed the explosives range to provide more routes and options.
- Changed the marksman slides to remove wind temporarily.
- Renamed "Tank Range" to "MBT Range".
- Renamed "IFV Course" to "Target Course".
- Renamed "Weapon Scopes" course to "Infantry Accuracy".
- Fixed RHS MBTs blocking spawning of new vehicles in the same spot.
- Fixed building with unopenable doors on the explosives range.
- Fixed "countours" typo on Land Navigation slide.
- Fixed out of bounds return trips to the land navigation teleporter.


**12SEP2017**
- Added two new Land Navigation routes.
- Moved start of Land Navigation Route 2. 
- Replaced UH Tiger Heavy with UH Tiger RMK Heavy.
- Removed Arsenal from Advanced Training area.
- Fixed incorrect Bounding tactics slide.
- Fixed missing marker options at CAS helipads.


**11JUL2017**
- Added IED slide to convoy slideshow.
- Updated RPG-7 scope example slide to include the OG-7V round.


**26JUN2017**
- Added third headless client slot.
- Added enableDebugConsole flag to description.ext.
- Fixed broken admin boards at the CAS Helipads.


**07JUN2017**
- Added Etiquette board to spawn position.
- Added third Hot LZ scenario to hostile target range.
- Added ability for vehicles to be spawned with engines turned on.
- Added Carrier Operations slide to B-grade rotary slideshow.
- Added paradrop dropzone to rotary fastroping & helocast markers.
- Added rearm and refuel point along fixed-wing taxiways.
- Changed CAS range vehicles to spawnables with engines running.
- Changed tank range vehicles to spawnables with engines running.
- Changed helipad markers to their own marker group.
- Changed A-grade fixed-wing slideshow order for better flow.
- Renamed fastroping and helocast markers from "LZ" to "DZ".
- Renamed "RESERVED" radio channel to "AIRNET".
- Fixed missing GPS targeting method in weapon type slides.
- Removed L-159 Alca from USS Freedom vehicle spawner.


**23MAY2017**
- Added teleport boards to rotary and fixed-wing drop zones.
- Added teleport boards to the joint tactics TACP positions.
- Replaced all swivel targets with pop-up targets on pistol range and killhouse.
- Moved fastroping landing zones so they no longer use building roofs.
- Fixed duplicate ACRE IDs on respawn by enabling reloading of player loadouts.
- Fixed "Fastroping ?" entry in configurable marker groups.
- Fixed clear vehicle inventories module not applying to spawned vehicles.
- Fixed all marker groups disappearing when a player joins the session.
- Fixed use of vanilla NATO units in hostile target range.


**21MAY2017**
- Added armory board to radio practical area.
- Added slide explaining the PGO-7V3 scope to weapon scopes slideshow.
- Added cones to driving course to indicate unclear route.
- Replaced artillery position with vehicle spawner of multiple types.
- Changed order of convoy vehicle spawn positions to be more logical.
- Fixed missing fire missions slide for mortar slideshow.
- Fixed typo in radio terminology slide for basic radio slideshow.


**20MAY2017**
- Fixed medical test patients not being injured or healed on a dedicated server.
- Fixed missing manual ranging slide for armor slideshow.
- Fixed missing IED slides for explosives slideshow.
- Fixed missing infantry tactics slide.
- Altered position of artillery guns.


**19MAY2017**
- Added GPS removal and restoration to land navigation teleporters.
- Added cargo area and markers for the air drop cargo spawn points.
- Added teleporters from Joint Tactics Flight School to Rotary and Fixed-Wing.
- Added BWI Armory boards to each of the firing range stands.
- Added Arsenal to the Armory boards located within the firing range area.
- Added loading screen image.
- Replaced all BWI Armory crates with infostand boards.
- Fixed missing marker admin board at the Joint Tactics Flight School.
- Fixed vehicle spawner failing on server due to simple object helipads.
- Fixed number of air drop cargo spawn points.
- Moved Training template to new project location.


**18MAY2017**
- Initial release.