## Introduction
This mission for use by members of [Black Watch International](http://blackwatch-int.com) for training and flight school operations. The latest version of the [BWI Modpack](https://gitlab.com/blackwatchint/bwi_modpack) is a requirement for using running this mission.

## Exporting
This mission can be exported from the EDEN editor by going to Scenario > Export to Multiplayer.

## Reporting Issues
Any issues with the mission should be reported on our [issue tracker](https://gitlab.com/blackwatchint/bwi_templates/issues). Please make sure to include the following information:
- Description of the issue.
- Steps to reproduce.
- Where the issue occurred (singleplayer/local multiplayer/dedicated server).
- The template or mission that the issue occurred on.
- Link to the RPT file if a crash or script error occurred. [How do I find my RPT file?](https://community.bistudio.com/wiki/Crash_Files#Arma_3)

## License
This mission, as well as the scripts and images within, is not licensed for use or duplication outside of the Black Watch International Arma 3 community.